using UnityEngine;

public class CalorieCalculator : MonoBehaviour
{
    // MET values for each activity
    private const float MET_PUNCHING = 6.0f;
    private const float MET_SQUATS = 5.0f;
    private const float MET_UP_PUNCH = 6.0f;
    private const float MET_DOWN_PUNCH = 6.0f;
    private const float MET_JUMP = 6.0f;

    // Player's weight in kilograms
    public float playerWeightKg = 70f;

    // Time tracking for each activity in seconds
    private float timePunching = 0f;
    private float timeSquats = 0f;
    private float timeUpPunch = 0f;
    private float timeDownPunch = 0f;
    private float timeJump = 0f;

    // Total calories burned
    private float totalCaloriesBurned = 0f;

    void Update()
    {
        // Simulate activities by updating the time spent on each activity
        // In a real game, this should be based on player actions

        timePunching += Time.deltaTime;
        timeSquats += Time.deltaTime;
        timeUpPunch += Time.deltaTime;
        timeDownPunch += Time.deltaTime;
        timeJump += Time.deltaTime;

        // Calculate calories burned continuously
        totalCaloriesBurned = CalculateCaloriesBurned(playerWeightKg, timePunching, timeSquats, timeUpPunch, timeDownPunch, timeJump);

        // Print the total calories burned to the console
        Debug.Log("Total calories burned: " + totalCaloriesBurned.ToString("F2"));
    }

    float CalculateCaloriesBurned(float weightKg, float timePunching, float timeSquats, float timeUpPunch, float timeDownPunch, float timeJump)
    {
        // Convert time from seconds to minutes
        float minutesPunching = timePunching / 60f;
        float minutesSquats = timeSquats / 60f;
        float minutesUpPunch = timeUpPunch / 60f;
        float minutesDownPunch = timeDownPunch / 60f;
        float minutesJump = timeJump / 60f;

        // Calculate calories burned for each activity
        float caloriesPunching = MET_PUNCHING * weightKg * minutesPunching;
        float caloriesSquats = MET_SQUATS * weightKg * minutesSquats;
        float caloriesUpPunch = MET_UP_PUNCH * weightKg * minutesUpPunch;
        float caloriesDownPunch = MET_DOWN_PUNCH * weightKg * minutesDownPunch;
        float caloriesJump = MET_JUMP * weightKg * minutesJump;

        // Total calories burned
        float totalCalories = caloriesPunching + caloriesSquats + caloriesUpPunch + caloriesDownPunch + caloriesJump;
        return totalCalories;
    }
}
