using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHandController : MonoBehaviour
{
    private List<Joycon> joycons;
    public int jc_ind;
    private Joycon joycon;
    //  public Transform upperArm; // Assign the upper arm bone in the Inspector
    //public Transform lowerArm; // Assign the lower arm bone in the Inspector

    // Adjust these parameters to control the sensitivity of arm movement and rotation limits
    public float gyroSensitivity = 20f;
    public float accelSensitivity = 100f;
    public float maxUpperArmRotation = 90f; // Maximum rotation allowed for the upper arm
    private Vector3 previousAccel;

    //
    public Animator animator;


    private bool isLeftBoxingPlaying = false;
    private bool isRightBoxingPlaying = false;
    private bool isUpperHit = false;
    private bool isHookAnimationPlaying = false;
    private bool isUpperCutPlaying = false;
    private bool isUpperLongPlaying = false;
    private bool isJumping = false;
    private bool isDucking = false;
    private bool isSquating = false;
    public GameObject SpawningPoints;

    //hand collider
    public GameObject handCollider;

    //
    private Vector3 lastAccel;
    private Vector3 lastGyro;
    private float punchThreshold = 2.0f; // Threshold for detecting a punch
    private float punchCooldown = 0.5f; // Cooldown time to prevent multiple punches
    private float lastPunchTime;

    void Start()
    {
        previousAccel = new Vector3(0.1f, 0.15f, 0.01f);
        joycons = JoyconManager.Instance.j;
        if (joycons.Count > 0)
        {
            joycon = joycons[jc_ind];
        }
    }
    void Update()
    {
        Vector3 gyro = joycon.GetGyro();
        Vector3 accel = joycon.GetAccel();
        /*        Debug.Log("gyro = " + gyro);
                Debug.Log("accel = " + accel);*/
       /* if (filteredValues[1] >= 5f && filteredValues[2] > 0 && !isLeftBoxingPlaying && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("Left_boxing"))
        {
            PlayAnimation("Left_boxing");
            Debug.Log(string.Format("sensorValues[]: {0}", string.Join(", ", sensorValues)));

        }*/
        //  Debug.Log(accel.magnitude);
        DetectPunch(gyro, accel);
        lastGyro = gyro;
        lastAccel = accel;
    }
    private void LogValues(Vector3 gyro, Vector3 accel)
    {
        // Log accelerometer and gyroscope values for fine-tuning
        Debug.Log($"Gyro: {gyro}");
    }
    void PlayAnimation(string animationName)
    {
        // Set the boolean parameter for the specified animation to true
        animator.SetBool(animationName, true);

        // Set the corresponding playing flag to true
        switch (animationName)
        {
            case "Left_boxing":
                isLeftBoxingPlaying = true;
                // Debug.Log("just checking true");
                break;
            case "right_hand":
                isRightBoxingPlaying = true;
                break;
            case "hook_animation":
                isHookAnimationPlaying = true;
                break;
            case "upper_cut":
                isUpperCutPlaying = true;
                break;
            case "upper_long":
                isUpperLongPlaying = true;
                break;
            case "upper_hit":
                isUpperHit = true;
                break;
            case "jump":
                isJumping = true;
                break;
            case "duck":
                isDucking = true;
                break;
            case "squat_hit":
                isSquating = true;
                break;
            default:
                break;
        }

        // Start the coroutine to reset the animation
        StartCoroutine(ResetAnimation(animationName));
    }
    IEnumerator ResetAnimation(string animationName)
    {
        // Wait until the current animation is finished
        yield return new WaitForSeconds(.7f);

        // Reset the boolean parameter for the specified animation
        animator.SetBool(animationName, false);
        //  Debug.Log("animation length " + animator.GetCurrentAnimatorStateInfo(0).length + " animation name " + animationName );
        // Reset the corresponding playing flag
        switch (animationName)
        {
            case "Left_boxing":
                isLeftBoxingPlaying = false;
                Debug.Log("just checking");
                break;
            case "right_hand":
                isRightBoxingPlaying = false;
                Debug.Log("just checking");
                break;
            case "hook_animation":
                isHookAnimationPlaying = false;
                break;
            case "upper_cut":
                isUpperCutPlaying = false;
                break;
            case "upper_long":
                isUpperLongPlaying = false;
                break;
            case "upper_hit":
                isUpperHit = false;
                break;
            case "jump":
                isJumping = false;
                break;
            case "duck":
                isDucking = false;
                break;
            case "squat_hit":
                isSquating = false;
                break;
            default:
                break;
        }
    }

    private void DetectPunch(Vector3 gyro, Vector3 accel)
    {
        // Check if enough time has passed since the last punch
        if (Time.time - lastPunchTime < punchCooldown) return;

        // Detect a punch by checking for a sudden increase in acceleration
        if (accel.magnitude > punchThreshold && IsPunchDirectionForward(gyro))
        {
            lastPunchTime = Time.time;
            Punch();
        }
    }

    private bool IsPunchDirectionForward(Vector3 gyro)
    {
        // Assuming the gyro's y-axis indicates forward motion.
        // Adjust the axis and threshold as per your specific requirements.
        // return gyro.y > 0.5f;
        return gyro.y > 0.5f;
    }

    private void Punch()
    {

        // right punch logic
        if(SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance == null)
        {
            return;
        }
       if( !isRightBoxingPlaying && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("Right_boxing"))
        {
            PlayAnimation("right_hand");
           // joycon.SetRumble(160, 320, 0.6f, 200);
        }
        else if (!isUpperHit && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("upper_hit"))
        {
            PlayAnimation("upper_hit");
            //joycon.SetRumble(160, 320, 0.6f, 200);

        }
        else if (!isUpperHit && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("squat_hit"))
        {
            PlayAnimation("squat_hit");
           // joycon.SetRumble(160, 320, 0.6f, 200);

        }

        Debug.Log("Punch detected!");

    }

    void EnableHandColider()
    {
        handCollider.GetComponent<BoxCollider>().enabled = true;
        
    }
    void DisableHandColider()
    {
        handCollider.GetComponent<BoxCollider>().enabled = false;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (isRightBoxingPlaying == true )
        {
            this.joycon.SetRumble(160, 320, 0.6f, 200);
        }
        else if(isUpperHit)
        {
            this.joycon.SetRumble(160, 320, 0.6f, 200);
        }

    }
}
