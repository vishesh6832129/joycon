using System.Collections.Generic;
using UnityEngine;

public class InstantiateOnOppositePlane : MonoBehaviour
{
    public List<GameObject> InstantiateObs;
    public GameObject objectToInstantiate; // The object to be instantiated
    public Collider planeCollider; // The collider of the plane
    public float instantiateInterval = 3.0f; // Time interval for instantiation

    private float timer = 0.0f;
    private int currentComboIndex = 0;
    private int comboRepeatCount = 0;
    private bool isFirstCombinationCompleted = false;

    public List<Transform> obsPos;
    public GameObject NewInstance;

    private List<(int, int)> instantiationCombinations = new List<(int, int)>();
    private List<int> singleInstantiationOrder = new List<int>();
    private int singleInstantiationIndex = 0;

    private void Start()
    {
        // Initial combination sequence setup
        SetupInstantiationCombinations();
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer >= instantiateInterval)
        {
            InstantiateObjectOnOppositePlane();
            timer = 0.0f;
        }
    }

    private void SetupInstantiationCombinations()
    {
        // Add combinations based on the description
        AddCombination(0, 1, 8); // First two objects alternately 16 times
        AddCombination(2, 3, 4); // Third and fourth objects alternately 8 times
      //  AddCombination(0, 1, 2); // First two objects alternately 8 times
        AddSingleInstantiation(4); // Fifth object once
        AddSingleInstantiation(0); // First object once
        AddSingleInstantiation(2); // Third object once
        AddSingleInstantiation(1); // Second object once
        AddSingleInstantiationSequence(new List<int> { 4, 0, 2, 1 }, 3); // 5th, 1st, 3rd, 2nd repeat 12 times

        /*// Repeat this combination 3 times
        List<(int, int)> tempCombinations = new List<(int, int)>(instantiationCombinations);
        for (int i = 0; i < 2; i++)
        {
            instantiationCombinations.AddRange(tempCombinations);
        }*/

        // Add the last combinations
        //    AddCombination(0, 1, 2); // First two objects alternately 16 times (increase speed in implementation)
        AddCombination(5,5,1); // Sixth object 8 times
    }

    private void AddCombination(int objIndex1, int objIndex2, int repeatCount)
    {
        for (int i = 0; i < repeatCount; i++)
        {
            instantiationCombinations.Add((objIndex1, objIndex2));
        }
    }

    private void AddSingleInstantiation(int objIndex)
    {
        singleInstantiationOrder.Add(objIndex);
    }

    private void AddSingleInstantiationSequence(List<int> objIndexes, int repeatCount)
    {
        for (int i = 0; i < repeatCount; i++)
        {
            singleInstantiationOrder.AddRange(objIndexes);
        }
    }

    public void InstantiateObjectOnOppositePlane()
    {
        if (objectToInstantiate == null || planeCollider == null)
        {
            Debug.LogError("Object to instantiate or plane collider not assigned.");
            return;
        }

        int selectedObjectIndex;

        if (currentComboIndex < instantiationCombinations.Count)
        {
            (int objIndex1, int objIndex2) = instantiationCombinations[currentComboIndex];
            selectedObjectIndex = (comboRepeatCount % 2 == 0) ? objIndex1 : objIndex2;

            // Update the combo repeat count and current combo index
            comboRepeatCount++;
            if (comboRepeatCount % 2 == 0)
            {
                currentComboIndex++;
                comboRepeatCount = 0;
            }
        }
        else
        {
            if (singleInstantiationIndex >= singleInstantiationOrder.Count)
            {
                if (!isFirstCombinationCompleted)
                {
                    isFirstCombinationCompleted = true;
                    RandomizeCombinations();
                    currentComboIndex = 0;
                    singleInstantiationIndex = 0;
                }
                else
                {
                    return; // All combinations have been processed
                }
            }
            selectedObjectIndex = singleInstantiationOrder[singleInstantiationIndex];
            singleInstantiationIndex++;
        }

        // Instantiate the object at the specified position
        NewInstance = Instantiate(InstantiateObs[selectedObjectIndex], obsPos[selectedObjectIndex].position, objectToInstantiate.transform.localRotation);
    }

    private void RandomizeCombinations()
    {
        System.Random rng = new System.Random();

        // Randomize instantiationCombinations
        int n = instantiationCombinations.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            var value = instantiationCombinations[k];
            instantiationCombinations[k] = instantiationCombinations[n];
            instantiationCombinations[n] = value;
        }

        // Randomize singleInstantiationOrder
        n = singleInstantiationOrder.Count;
        while (n > 1)
        {
            n--;
            int k = rng.Next(n + 1);
            int value = singleInstantiationOrder[k];
            singleInstantiationOrder[k] = singleInstantiationOrder[n];
            singleInstantiationOrder[n] = value;
        }
    }
}
