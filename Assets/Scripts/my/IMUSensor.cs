using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IMUData
{
    public Vector3 gyro;
    public Vector3 accelerometer;
    public Quaternion orientation;
}
public class IMUSensor : MonoBehaviour
{
    public IMUData imuData;

    void Update()
    {
        Quaternion orientation = imuData.orientation;
        Vector3 euler = orientation.eulerAngles;

        float pitch = euler.x;
        float roll = euler.z;
        float yaw = euler.y;

        Debug.Log($"Pitch: {pitch}, Roll: {roll}, Yaw: {yaw}");
    }
}
public class ComplementaryFilter
{
    private float alpha;
    private Vector3 filteredAccel;

    public ComplementaryFilter(float alpha)
    {
        this.alpha = alpha;
    }

    public Vector3 Filter(Vector3 accel, Vector3 gyro, float dt)
    {
        Vector3 gyroAngle = gyro * dt;
        filteredAccel = alpha * (filteredAccel + gyroAngle) + (1 - alpha) * accel;
        return filteredAccel;
    }
}
public class ObjectMovement : MonoBehaviour
{
    public IMUData imuData;
    private ComplementaryFilter filter;
    private Vector3 position;
    private Vector3 velocity;

    void Start()
    {
        filter = new ComplementaryFilter(0.98f);
        position = Vector3.zero;
        velocity = Vector3.zero;
    }

    void Update()
    {
        float dt = Time.deltaTime;

        Vector3 accel = imuData.accelerometer;
        Vector3 gyro = imuData.gyro;

        // Apply the filter to get a more accurate acceleration
        Vector3 filteredAccel = filter.Filter(accel, gyro, dt);

        // Integrate acceleration to get velocity
        velocity += filteredAccel * dt;

        // Integrate velocity to get position
        position += velocity * dt;

        // Apply the new position to the object
        transform.position = position;

        Debug.Log($"Position: {position}, Velocity: {velocity}");
    }
}

