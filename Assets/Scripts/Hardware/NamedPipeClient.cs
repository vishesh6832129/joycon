using Filters;
using PimDeWitte.UnityMainThreadDispatcher;
using System;
using System.Collections;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UIElements;

public class NamedPipeClient : MonoBehaviour
{
    private NamedPipeClientStream pipeClient;
    private StreamReader streamReader;
    private System.Threading.Thread readThread;
    StringBuilder stringBuilder;
    public float wValue;

    private Vector3 velocity;
    private Vector3 displacement;
    public float test;
    float[] sensorValues;
    public Animator animator;
  

    private bool isLeftBoxingPlaying = false;
    private bool isHookAnimationPlaying = false;
    private bool isUpperCutPlaying = false;
    private bool isUpperLongPlaying = false;
    public GameObject SpawningPoints;

    //hand collider
    public GameObject handCollider;


    private KalmanFilter m_Filter;
    float[] filteredValues;
    private void Start()
    {
        Debug.Log("Start Testing Filter");
        m_Filter = new KalmanFilter();
        m_Filter.State = 0; //Setting first (non filtered) value to 0 for example;
        // Start the ReadPipe method in a new thread
        readThread = new System.Threading.Thread(ReadPipe);
        readThread.Start();
        StringBuilder stringBuilder = new StringBuilder();
    }

    private void Update()
    {
        if (sensorValues == null)
        {
            return;
        }
        else
        {
            // Debug.Log(SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.tag.ToString());
            // Check conditions for each animation
            /*if (sensorValues[1] >= 0f && !isLeftBoxingPlaying && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("Left_boxing"))
            {
                PlayAnimation("Left_boxing");
                Debug.Log(string.Format("sensorValues[]: {0}", string.Join(", ", sensorValues)));

            }*/
            if (filteredValues[1] >= 5f && filteredValues[2] > 0 && !isLeftBoxingPlaying /*&& SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("Left_boxing")*/)
            {
                PlayAnimation("Left_boxing");
                Debug.Log(string.Format("sensorValues[]: {0}", string.Join(", ", sensorValues)));

            }
            else if (filteredValues[1] >= 20f && filteredValues[2] <= -10 && filteredValues[5] > 5/*&& SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("hook_animation")*/ && !isLeftBoxingPlaying)
            {
                PlayAnimation("hook_animation");
                Debug.Log(string.Format("sensorValues[]: {0}", string.Join(", ", sensorValues)));

            }
            else if (filteredValues[0] <= -15 && filteredValues[2] <= -15 && filteredValues[5] < 3 &&!isUpperCutPlaying /*&& SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("upper_cut")*/)
            {
                PlayAnimation("upper_cut");
                Debug.Log(string.Format("sensorValues[]: {0}", string.Join(", ", sensorValues)));
            }
            /*else if (sensorValues[4] >= -2 && sensorValues[4] <= 3f && !isUpperLongPlaying)
            {
                PlayAnimation("upper_long");
            }*/

        }
       /* if (sensorValues == null)
        {
            return;
        }
        else
        {
            // Debug.Log(SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.tag.ToString());
            // Check conditions for each animation
            if (IsLeftPunch(sensorValues) && !isLeftBoxingPlaying)
            {
                PlayAnimation("LeftPunch");
            }
            else if (sensorValues[0] >= -8 && sensorValues[0] <= -6f && !isHookAnimationPlaying && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("hook_animation"))
            {
                PlayAnimation("hook_animation");
            }
            else if (sensorValues[0] >= 2 && sensorValues[1] <= 4f && !isUpperCutPlaying && SpawningPoints.GetComponent<InstantiateOnOppositePlane>().NewInstance.CompareTag("upper_cut"))
            {
                PlayAnimation("upper_cut");
            }
            *//* else if (sensorValues[4] >= -2 && sensorValues[4] <= 3f && !isUpperLongPlaying)
             {
                 PlayAnimation("upper_long");
             }*//*

        }*/
    }

    /*bool IsLeftPunch(float[] sensorValues)
    {
        // Define thresholds for gyroscope readings
        float gyroThreshold1 = 20f;  // Threshold for the first gyroscope reading
        float gyroThreshold2 = 20f;  // Threshold for the second gyroscope reading
        float gyroThreshold3 = 20f;  // Threshold for the third gyroscope reading

        // Check if gyroscope readings exceed thresholds
        return (sensorValues[0] >= gyroThreshold1 &&
                sensorValues[1] >= gyroThreshold2 &&
                sensorValues[2] >= gyroThreshold3);
    }*/
    void PlayAnimation(string animationName)
    {
        // Set the boolean parameter for the specified animation to true
        animator.SetBool(animationName, true);

        // Set the corresponding playing flag to true
        switch (animationName)
        {
            case "Left_boxing":
                isLeftBoxingPlaying = true;
               // Debug.Log("just checking true");
                break;
            case "hook_animation":
                isHookAnimationPlaying = true;
                break;
            case "upper_cut":
                isUpperCutPlaying = true;
                break;
            case "upper_long":
                isUpperLongPlaying = true;
                break;
            default:
                break;
        }

        // Start the coroutine to reset the animation
        StartCoroutine(ResetAnimation(animationName));
    }
    IEnumerator ResetAnimation(string animationName)
    {
        // Wait until the current animation is finished
        yield return new WaitForSeconds(.2f);

        // Reset the boolean parameter for the specified animation
        animator.SetBool(animationName, false);
      //  Debug.Log("animation length " + animator.GetCurrentAnimatorStateInfo(0).length + " animation name " + animationName );
        // Reset the corresponding playing flag
        switch (animationName)
        {
            case "Left_boxing":
                isLeftBoxingPlaying = false;
                Debug.Log("just checking");
                break;
            case "hook_animation":
                isHookAnimationPlaying = false;
                break;
            case "upper_cut":
                isUpperCutPlaying = false;
                break;
            case "upper_long":
                isUpperLongPlaying = false;
                break;
            default:
                break;
        }
    }
    private void OnDestroy()
    {
        // Close the StreamReader and NamedPipeClientStream
        if (streamReader != null)
        {
            streamReader.Close();
            streamReader = null;
        }

        if (pipeClient != null)
        {
            pipeClient.Close();
            pipeClient = null;
        }

        // Safely abort the read thread if it's running
        if (readThread != null && readThread.IsAlive)
        {
            readThread.Abort();
            readThread = null;
        }
    }

    private float[] FilterGyroscopeData(float[] sensorValues)
    {
        float[] filteredValues = new float[sensorValues.Length];

        for (int i = 0; i < sensorValues.Length; i++)
        {
            // Apply filter to each sensor value
            filteredValues[i] = m_Filter.FilterValue(sensorValues[i]);
        }

        return filteredValues;
    }
    //rotation working
    private void PlayerHandPosition(string data)
    {
        // Remove brackets and split the string into individual values
        string[] values = data.Trim('[', ']').Split(',');

        // Convert the string values to floats
         sensorValues = new float[values.Length];
        for (int i = 0; i < values.Length; i++)
        {
            sensorValues[i] = float.Parse(values[i]);
        }

        // Extract gyroscope values
        Vector3 gyroscopeValues = new Vector3(sensorValues[0], sensorValues[1], sensorValues[2]);

        // Extract accelerometer values
        Vector3 accelerometerValues = new Vector3(sensorValues[3], sensorValues[4], sensorValues[5]);
         filteredValues = FilterGyroscopeData(sensorValues);
       // Test(sensorValues[0]);
        // Define a dead zone threshold to filter out small values
        float deadZoneThreshold = 0.1f;

        // Use MainThreadDispatcher to execute Unity-related code on the main thread
        UnityMainThreadDispatcher.Instance().Enqueue(() =>
        {
            // transform.rotation = new Quaternion(-sensorValues[1], -sensorValues[2], sensorValues[0], 0);
            /* if (sensorValues[1] >= -2 && sensorValues[1] <= 0f)
             {
                 animator.SetBool("Left_boxing", true);
             }
             else
             {
                 animator.SetBool("Left_boxing", false);

             }*/
            
           /* if (sensorValues[1] >= -2 && sensorValues[1] <= 3f )
            {
                // Check if the animation has not been triggered yet
               
                    // Set the boolean parameter to true to trigger the animation
                    animator.SetBool("Left_boxing", true);
                    StartCoroutine(ResetLeftBoxing());

                  
                   
                   
                
            }
            else
            {
               
                // Reset the boolean parameter to false if the condition is not met
                animator.SetBool("Left_boxing", false);

              
            }*/
        });
    }
  
    private void ReadPipe()
    {
        try
        {
            pipeClient = new NamedPipeClientStream(".", "ABC", PipeDirection.In);
            pipeClient.Connect();
            Debug.Log("Connected to pipe.");

            streamReader = new StreamReader(pipeClient);
            while (true)
            {
                string line = streamReader.ReadLine();
                if (line == null)
                {
                    // Exit the loop if there's no more data
                    break;
                }

                // Log the entire line
                string temp = line;
                Debug.Log(temp);
                
                PlayerHandPosition(temp);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Error: " + e.Message);
        }
    }

    //hand collider enable

    


}
