using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyInstantiatedObject : MonoBehaviour
{
    public GameObject instantiatedObject;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        if(instantiatedObject == null)
        {
            return;
        }
        else if(this.instantiatedObject.activeSelf == false  || transform.GetChild(0).transform.position.z < -47 )
        {
            Destroy(gameObject);
        }
    }
}
