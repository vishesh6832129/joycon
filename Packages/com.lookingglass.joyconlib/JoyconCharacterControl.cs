using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyconCharacterControl : MonoBehaviour
{
    private List<Joycon> joycons;
    public int jc_ind = 0;
    private Joycon joycon;
    public Animator animator;

    private Vector3 previousAccel;

    // Sensitivity settings
    public float accelThreshold = 0.1f; // Adjust this value to set the sensitivity for the z-axis movement detection

    // Flags for animation state
    private bool isLeftBoxingPlaying = false;

    void Start()
    {
        // Initialize previous acceleration with the current acceleration value
        previousAccel = new Vector3(0.1f, 0.1f, 0.1f);

        // Get the list of Joycons
        joycons = JoyconManager.Instance.j;
        if (joycons.Count > 0)
        {
            joycon = joycons[jc_ind];
        }
    }

    void Update()
    {
        // Check if Joycon is available
        if (joycons == null || joycons.Count <= jc_ind) return;

        Vector3 accel = joycon.GetAccel();

        // Check if the change in the z-axis acceleration indicates forward movement and exceeds the threshold
        if (previousAccel != Vector3.zero && (accel.z - previousAccel.z) < -accelThreshold)
        {
            PlayAnimation("Left_boxing");
            joycon.SetRumble(160, 320, 0.6f, 200);
        }

        // Update the previous acceleration value
        previousAccel = accel;
    }

    void PlayAnimation(string animationName)
    {
        // Play the animation if it's not already playing
        if (!isLeftBoxingPlaying && animationName == "Left_boxing")
        {
            animator.SetBool(animationName, true);
            isLeftBoxingPlaying = true;
            StartCoroutine(ResetAnimation(animationName, 1.0f)); // Adjust the duration to match your animation length
        }
    }

    IEnumerator ResetAnimation(string animationName, float delay)
    {
        yield return new WaitForSeconds(delay);
        animator.SetBool(animationName, false);
        isLeftBoxingPlaying = false;
    }
}
