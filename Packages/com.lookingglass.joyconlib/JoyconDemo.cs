﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoyconDemo : MonoBehaviour
{

    private List<Joycon> joycons;

    // Values made available via Unity
    public float[] stick;
    public Vector3 gyro;
    public Vector3 accel;
   // public Vector3 tempvalue;
    public int jc_ind = 0;
    public Quaternion orientation;
    // Additional variables for object movement
    public float moveSpeed = 5f;
    void Start()
    {
        // Initialize gyro and accel to zero
        gyro = Vector3.zero;
        accel = Vector3.zero;

        // Reset position and rotation of the GameObject
        transform.position = Vector3.zero;
        transform.rotation = Quaternion.identity;
        gyro = new Vector3(0, 0, 0);
        accel = new Vector3(0, 0, 0);
       // tempvalue = new Vector3(0.07f, -0.03f, -1.02f);
       // get the public Joycon array attached to the JoyconManager in scene
        joycons = JoyconManager.Instance.j;
        if (joycons.Count < jc_ind + 1)
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // make sure the Joycon only gets checked if attached
        if (joycons.Count > 1)
        {
                Joycon j = joycons[jc_ind];

                // GetButtonDown checks if a button has been pressed (not held)
                if (j.GetButtonDown(Joycon.Button.SHOULDER_2))
                {
                    Debug.Log("Shoulder button 2 pressed");
                    // GetStick returns a 2-element vector with x/y joystick components
                    Debug.Log(string.Format("Stick x: {0:N} Stick y: {1:N}", j.GetStick()[0], j.GetStick()[1]));

                    // Joycon has no magnetometer, so it cannot accurately determine its yaw value. Joycon.Recenter allows the user to reset the yaw value.
                    j.Recenter();
                }
                // GetButtonDown checks if a button has been released
                if (j.GetButtonUp(Joycon.Button.SHOULDER_2))
                {
                    Debug.Log("Shoulder button 2 released");
                }
                // GetButtonDown checks if a button is currently down (pressed or held)
                if (j.GetButton(Joycon.Button.SHOULDER_2))
                {
                    Debug.Log("Shoulder button 2 held");
                }

                if (j.GetButtonDown(Joycon.Button.MINUS))
                {
                    Debug.Log("Rumble");

                    // Rumble for 200 milliseconds, with low frequency rumble at 160 Hz and high frequency rumble at 320 Hz. For more information check:
                    // https://github.com/dekuNukem/Nintendo_Switch_Reverse_Engineering/blob/master/rumble_data_table.md

                    j.SetRumble(160, 320, 0.6f, 200);

                    // The last argument (time) in SetRumble is optional. Call it with three arguments to turn it on without telling it when to turn off.
                    // (Useful for dynamically changing rumble values.)
                    // Then call SetRumble(0,0,0) when you want to turn it off.
                }
                // Move GameObject based on Joycon values
                MoveObject(j);

                stick = j.GetStick();

                // Gyro values: x, y, z axis values (in radians per second)
                gyro = j.GetGyro();

                // Accel values:  x, y, z axis values (in Gs)
                accel = j.GetAccel();

                //    orientation = j.GetVector();
                //    if (j.GetButton(Joycon.Button.DPAD_UP))
                //    {
                //        gameObject.GetComponent<Renderer>().material.color = Color.red;
                //    }
                //    else
                //    {
                //        gameObject.GetComponent<Renderer>().material.color = Color.blue;
                //    }
                //    gameObject.transform.rotation = orientation;
            }
        

    }
    void MoveObject(Joycon joycon)
    {
        // Calculate movement based on gyro, accel, and orientation values
        Vector3 moveDirection = new Vector3(0f, 0f, joycon.GetGyro().z);

        // Apply additional acceleration based on accel values
        moveDirection += joycon.GetAccel() * 2f;

        // Normalize the direction vector to prevent faster diagonal movement
        moveDirection.Normalize();

        // Move the GameObject
        transform.Translate(moveDirection * moveSpeed * Time.deltaTime);

        // Update gyro and accel values
        gyro = joycon.GetGyro();
        accel = joycon.GetAccel();
    }
}